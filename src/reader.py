__all__ = ['read_lines', 'read_json']

from functools import wraps
from typing import Callable


def read_lines(path: str, *, filter_empty=True):
    from typing import List

    def decorator(func: Callable[..., List[str]]):
        @wraps(func)
        def wrapper(*args, **kwargs):
            with open(path, 'r') as f:
                lines = f.readlines()
                if filter_empty:
                    lines = [line for line in lines if line.strip()]
                return func(*args, **kwargs, lines=lines)

        return wrapper

    return decorator


def read_json(path: str):
    from typing import Dict

    def decorator(func: Callable[..., Dict]):
        @wraps(func)
        def wrapper(*args, **kwargs):
            import json
            with open(path, 'r') as f:
                jsn = json.loads(f.read())
                return func(*args, json=jsn, **kwargs)

        return wrapper

    return decorator
